package com.curos.oauth;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
public class OAuthController {
	@Autowired
	private TokenStore tokenStore;

	@RequestMapping(value = "/oauth/revoke")
	@ResponseStatus(HttpStatus.OK)
	public void removeAccessToken(HttpServletRequest request, @RequestParam(name = "clientId") String clientId) {
		Collection<OAuth2AccessToken> tokenList = tokenStore.findTokensByClientId(clientId);
		for (OAuth2AccessToken oAuth2AccessToken : tokenList) {
			tokenStore.removeAccessToken(oAuth2AccessToken);
		}
	}
}